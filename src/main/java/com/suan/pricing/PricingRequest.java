package com.suan.pricing;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PricingRequest {

    @JsonProperty("flood_factor")
    private int floodFactor;

    @JsonProperty("bldg_safety_factor")
    private int buildingSafetyFactor;

    @JsonProperty
    private String location;

    public int getFloodFactor() {
        return floodFactor;
    }

    public void setFloodFactor(int floodFactor) {
        this.floodFactor = floodFactor;
    }

    public int getBuildingSafetyFactor() {
        return buildingSafetyFactor;
    }

    public void setBuildingSafetyFactor(int buildingSafetyFactor) {
        this.buildingSafetyFactor = buildingSafetyFactor;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}
