package com.suan.pricing;

import java.util.LinkedHashSet;
import java.util.Set;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "services")
public class ServicesProperties {

    private SmartRating smartRating;
    private ExcellentRating excellentRating;
    private IDoEverythingRating iDoEverythingRating;
    
    public SmartRating getSmartRating() {
        return smartRating;
    }

    public void setSmartRating(SmartRating smartRating) {
        this.smartRating = smartRating;
    }

    public ExcellentRating getExcellentRating() {
        return excellentRating;
    }

    public void setExcellentRating(ExcellentRating excellentRating) {
        this.excellentRating = excellentRating;
    }
    
    public IDoEverythingRating getIDoEverythingRating() {
        return iDoEverythingRating;
    }
    
    public void setIDoEverythingRating(IDoEverythingRating iDoEverythingRating) {
        this.iDoEverythingRating = iDoEverythingRating;
    }

    public static class SmartRating {
        private int priority;
        private Set<String> supportedLocations = new LinkedHashSet<>();

        public int getPriority() {
            return priority;
        }
        
        public void setPriority(int priority) {
            this.priority = priority;
        }

        public Set<String> getSupportedLocations() {
            return supportedLocations;
        }
    }

    public static class ExcellentRating {
        private int priority;
        private Set<String> supportedLocations = new LinkedHashSet<>();

        public int getPriority() {
            return priority;
        }
        
        public void setPriority(int priority) {
            this.priority = priority;
        }

        public Set<String> getSupportedLocations() {
            return supportedLocations;
        }
    }
    
    public static class IDoEverythingRating {
        private int priority;
        private Set<String> supportedLocations = new LinkedHashSet<>();

        public int getPriority() {
            return priority;
        }
        
        public void setPriority(int priority) {
            this.priority = priority;
        }

        public Set<String> getSupportedLocations() {
            return supportedLocations;
        }
    }

}
