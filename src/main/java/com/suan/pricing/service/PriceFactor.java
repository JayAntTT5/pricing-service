package com.suan.pricing.service;

import java.util.Objects;

public final class PriceFactor {
    private int floodFactor;
    private int buildingSafetyFactor;
    private String location;
    
    private PriceFactor() {}

    public static PriceFactor of(int floodFactor, int buildingSafetyFactor, String location) {
        PriceFactor factor = new PriceFactor();
        factor.floodFactor = floodFactor;
        factor.buildingSafetyFactor = buildingSafetyFactor;
        factor.location = Objects.requireNonNull(location);
        return factor;
    }

    public final int getFloodFactor() {
        return floodFactor;
    }

    public final int getBuildingSafetyFactor() {
        return buildingSafetyFactor;
    }

    public final String getLocation() {
        return location;
    }

}
