package com.suan.pricing.service;

import java.util.Comparator;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PricingServiceFactory {
    
    @Autowired
    private Set<PricingService> services;
    
    @Autowired
    private PricingService defaultPricingService;
    
    public PricingService getServiceForLocation(String location) {
        return services.stream()
                .sorted(Comparator.comparingInt(PricingService::getPriority))
                .filter(service -> service.getSupportedLocations()
                        .stream().anyMatch(s -> s.equalsIgnoreCase(location)))
                .findFirst()
                .orElse(defaultPricingService);
    }
    
}
