package com.suan.pricing.service;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.suan.pricing.ServicesProperties;

@Service
public class SmartRatingService implements PricingService {

    @Autowired
    private ServicesProperties servicesProperties;

    @Override
    public Price calculatePrice(PriceFactor factor) {
        return Price.of(new BigDecimal("1.00"), new BigDecimal("1.00"), new BigDecimal("1.00"));
    }

    @Override
    public int getPriority() {
        return servicesProperties.getSmartRating().getPriority();
    }

    @Override
    public Set<String> getSupportedLocations() {
        return Collections.unmodifiableSet(
                servicesProperties.getSmartRating().getSupportedLocations());
    }

}
