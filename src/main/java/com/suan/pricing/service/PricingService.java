package com.suan.pricing.service;

import java.util.Set;

public interface PricingService {
    Price calculatePrice(PriceFactor factor);
    int getPriority();
    Set<String> getSupportedLocations();
}
