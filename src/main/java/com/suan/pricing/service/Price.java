package com.suan.pricing.service;

import static java.util.Objects.requireNonNull;

import java.math.BigDecimal;

public class Price {

    private BigDecimal grossPrice;
    private BigDecimal technicalPrice;
    private BigDecimal netPrice;
    
    private Price() {}
    
    public static Price of(BigDecimal grossPrice, BigDecimal technicalPrice, BigDecimal netPrice) {
        Price price = new Price();
        price.grossPrice = requireNonNull(grossPrice).setScale(2);
        price.technicalPrice = requireNonNull(technicalPrice).setScale(2);
        price.netPrice = requireNonNull(netPrice).setScale(2);
        return price;
    }

    public final BigDecimal getGrossPrice() {
        return grossPrice;
    }

    public final BigDecimal getTechnicalPrice() {
        return technicalPrice;
    }

    public final BigDecimal getNetPrice() {
        return netPrice;
    }

}
