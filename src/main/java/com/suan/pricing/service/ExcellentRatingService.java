package com.suan.pricing.service;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.suan.pricing.ServicesProperties;

@Service
class ExcellentRatingService implements PricingService {

    @Autowired
    private RestTemplate restTemplate;
    
    @Autowired
    private ServicesProperties servicesProperties;
    
    @Override
    public Price calculatePrice(PriceFactor factor) {
        return Price.of(new BigDecimal("2.00"), new BigDecimal("2.00"), new BigDecimal("2.00"));
    }

    @Override
    public int getPriority() {
        return servicesProperties.getExcellentRating().getPriority();
    }

    @Override
    public Set<String> getSupportedLocations() {
        return Collections.unmodifiableSet(
                servicesProperties.getExcellentRating().getSupportedLocations());
    }

}
