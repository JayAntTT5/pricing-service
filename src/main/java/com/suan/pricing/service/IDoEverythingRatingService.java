package com.suan.pricing.service;

import java.math.BigDecimal;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.suan.pricing.ServicesProperties;

/**
 * This service will act as fallback if all other services
 * do not support a certain location
 *
 */
@Service
@Primary
class IDoEverythingRatingService implements PricingService {

    @Autowired
    private ServicesProperties servicesProperties;

    @Override
    public Price calculatePrice(PriceFactor factor) {
        return Price.of(new BigDecimal("3.00"), new BigDecimal("3.00"), new BigDecimal("3.00"));
    }

    @Override
    public int getPriority() {
        return servicesProperties.getIDoEverythingRating().getPriority();
    }

    @Override
    public Set<String> getSupportedLocations() {
        return servicesProperties.getIDoEverythingRating().getSupportedLocations();
    }

}
