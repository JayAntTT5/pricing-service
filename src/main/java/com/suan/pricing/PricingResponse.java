package com.suan.pricing;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.suan.pricing.service.Price;

public class PricingResponse {

    @JsonProperty("gross_price")
    private BigDecimal grossPrice;

    @JsonProperty("technical_price")
    private BigDecimal technicalPrice;

    @JsonProperty("net_price")
    private BigDecimal netPrice;

    public static PricingResponse from(Price price) {
        PricingResponse response = new PricingResponse();
        response.grossPrice = price.getGrossPrice();
        response.technicalPrice = price.getTechnicalPrice();
        response.netPrice = price.getNetPrice();
        return response;
    }

    public BigDecimal getGrossPrice() {
        return grossPrice;
    }

    public void setGrossPrice(BigDecimal grossPrice) {
        this.grossPrice = grossPrice;
    }

    public BigDecimal getTechnicalPrice() {
        return technicalPrice;
    }

    public void setTechnicalPrice(BigDecimal technicalPrice) {
        this.technicalPrice = technicalPrice;
    }

    public BigDecimal getNetPrice() {
        return netPrice;
    }

    public void setNetPrice(BigDecimal netPrice) {
        this.netPrice = netPrice;
    }

}
