package com.suan.pricing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.suan.pricing.service.Price;
import com.suan.pricing.service.PriceFactor;
import com.suan.pricing.service.PricingService;
import com.suan.pricing.service.PricingServiceFactory;

@RestController
@RequestMapping("pricing")
public class PricingController {

    @Autowired
    private PricingServiceFactory serviceFactory;

    @PostMapping("calculate")
    public PricingResponse calculatePrice(@RequestBody PricingRequest request) {
        PricingService service = serviceFactory.getServiceForLocation(request.getLocation());
        Price calculatedPrice = service.calculatePrice(
                PriceFactor.of(request.getFloodFactor(),
                        request.getBuildingSafetyFactor(),
                        request.getLocation()));
        return PricingResponse.from(calculatedPrice);
    }
}
