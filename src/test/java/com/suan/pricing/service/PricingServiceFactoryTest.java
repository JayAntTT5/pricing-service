package com.suan.pricing.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import com.suan.pricing.ServicesProperties;
import com.suan.pricing.ServicesProperties.ExcellentRating;
import com.suan.pricing.ServicesProperties.IDoEverythingRating;
import com.suan.pricing.ServicesProperties.SmartRating;

@RunWith(SpringRunner.class)
@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
@ContextConfiguration(classes = PricingServiceFactoryTest.TestConfig.class)
public class PricingServiceFactoryTest {

    @MockBean
    private ServicesProperties servicesProperties;

    @Autowired
    private PricingServiceFactory serviceFactory;

    @Before
    public void setUp() {
        SmartRating smartRatingProps = new SmartRating();
        smartRatingProps.setPriority(1);
        smartRatingProps.getSupportedLocations().add("Pasig");
        smartRatingProps.getSupportedLocations().add("Caloocan");
        given(servicesProperties.getSmartRating()).willReturn(smartRatingProps);

        ExcellentRating excellentRatingProps = new ExcellentRating();
        excellentRatingProps.setPriority(2);
        excellentRatingProps.getSupportedLocations().add("QC");
        excellentRatingProps.getSupportedLocations().add("Caloocan");
        given(servicesProperties.getExcellentRating()).willReturn(excellentRatingProps);

        IDoEverythingRating iDoEverythingRatingProps = new IDoEverythingRating();
        iDoEverythingRatingProps.setPriority(3);
        iDoEverythingRatingProps.getSupportedLocations().add("*");
        given(servicesProperties.getIDoEverythingRating()).willReturn(iDoEverythingRatingProps);
    }

    @Test
    public void shouldReturnSmartRatingService_givenLocationIsPasig() {
        PricingService service = serviceFactory.getServiceForLocation("Pasig");

        assertThat(service.getPriority()).isEqualTo(1);
        assertThat(service.getSupportedLocations()).contains("Pasig");
    }

    @Test
    public void shouldReturnServiceWithHighestPriority_twoOrMoreServiceSupportsSameLocation() {
        PricingService service = serviceFactory.getServiceForLocation("Pasig");

        assertThat(service.getPriority()).isEqualTo(1); // SmartRatingService
        assertThat(service.getSupportedLocations()).contains("Pasig", "Caloocan");
    }

    @Test
    public void shouldReturnDefaultService_allOthersDoNotSupportALocation() {
        PricingService service = serviceFactory.getServiceForLocation("Malabon");

        assertThat(service.getPriority()).isEqualTo(3); // IDoEverythingRatingService
        assertThat(service.getSupportedLocations()).contains("*");
    }

    @TestConfiguration
    @ComponentScan
    static class TestConfig {

        @Bean
        public RestTemplate restTemplate() {
            return new RestTemplate();
        }

    }

}
