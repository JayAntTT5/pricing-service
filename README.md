## Requirements
* Java 8+


## Instructions
1. Build project `./mvnw package`
2. Run jar `java -jar target/pricing-service.jar`
3. Open REST client and do POST

```
{
  "flood_factor": <int>,
  "bldg_safety_factor": <int>,
  "location": <string>
}
```

## Hardcoded Responses
* SmartRatingService 

```
{
    "gross_price": 1,
    "technical_price": 1,
    "net_price": 1
}
```

* ExcellentRatingService

```
{
    "gross_price": 2,
    "technical_price": 2,
    "net_price": 2
}
```

* IDoEverythingRatingService

```
{
    "gross_price": 3,
    "technical_price": 3,
    "net_price": 3
}
```


## Few notes
* SOAP-related source files and other external web service related code are not written since they are all generalized with interface `PricingService`. One can easily add request and response POJOs to those services without modifications to client code.

